import sys
import prog4_1
import prog4_2

def main():
  print("Assignment #4-3, Edward Mun, eddiejmun@gmail.com")
  file = sys.argv[1]
  array = []
  lines = []
  with open(file, "r") as myfile:
    string  = [x.strip() for x in myfile.readlines()]

    for i in range (0,len(string)):
      arr = prog4_1.Tokenize(string[i])
      prog4_1.Parse(arr)
  stack = prog4_2.StackMachine()
  while(stack.CurrentLine < len(string)):
    if(stack.CurrentLine < 0):
      print("Trying to execute invalid line:",stack.CurrentLine)
      return
    try:
      done = stack.Execute(string[stack.CurrentLine])
      if (done != None):
        print(done)
    except IndexError as e:
      num = ((stack.CurrentLine) + 1)
      print("Line #%s:"%int(num),"'%s'"%stack.error,"caused",e)
      return
  print("Program terminated correctly")
  quit()
    
  




if __name__ == "__main__":
    main()
