import sys
import prog4_1
class StackMachine:
  
  def __init__(self):
    self.data = []
    self.CurrentLine = 0
    self.saved = [0,0,0,0,0,0]
    self.error = 0

  def Execute(self,lines):
      tokens = lines.split()
      for i in range(0,len(tokens)):
        if tokens[i] == "push":
          self.CurrentLine += 1
          self.push(tokens[i+1])
        if tokens[i] == "pop":
          self.CurrentLine += 1
          return self.pop()
        if tokens[i] == "sub":
          self.sub()
          self.CurrentLine += 1
        if tokens[i] == "add":
          self.add()
          self.CurrentLine += 1
        if tokens[i] == "div":
          self.div()
          self.CurrentLine += 1
        if tokens[i] == "mul":
          self.mul()
          self.CurrentLine += 1
        if tokens[i] == "mod":
          self.mod()
          self.CurrentLine += 1
        if tokens[i] == "skip":
          self.skip()
          self.CurrentLine += 1
        if tokens[i] == "save":
          self.save(tokens[i+1])
          self.CurrentLine += 1 
        if tokens[i] == "get":
          self.get(tokens[i+1])
          self.CurrentLine += 1
        
      
      

    
    



  def pop(self):
    if (len(self.data)) == 0:
      self.error = "pop"
      raise IndexError("Invalid Memory Access")
    else:
      test = self.data.pop()
      return test
  
  def push(self,x):
    self.data.append(x)
    return None
        
  def add(self):
    if (len(self.data)) < 2:
      self.error = "add"
      raise IndexError("Invalid Memory Access")
    else:
      a = self.pop()
      b = self.pop()
      c = int(a) + int(b)
      self.push(c)
      return None
        
  def sub(self):
    if (len(self.data)) < 2:
      self.error = "sub"
      raise IndexError("Invalid Memory Access")
    else:
      a = self.pop()
      b = self.pop()
      c = int(a) - int(b)
      self.push(c)
      return None
        
  def mul(self):
    if (len(self.data)) < 2:
      self.error = "mul"
      raise IndexError("Invalid Memory Access")
    else:
      a = self.pop()
      b = self.pop()
      c = int(a) * int(b)
      self.push(c)
      return None
        
  def div(self):
    if (len(self.data)) < 2:
      self.error = "div"
      raise IndexError("Invalid Memory Access")
    else:
      a = self.pop()
      b = self.pop()
      c = int(a) / int(b)
      self.push(c)
      return None
        
  def mod(self):
    if (len(self.data)) < 2:
      self.error = "mod"
      raise IndexError("Invalid Memory Access")
      return None
    else:
      a = self.pop()
      b = self.pop()
      c = int(a) % int(b)
      self.push(c)
      return None
  
  def skip(self):
    if (len(self.data)) < 2:
      self.error = "skip"
      raise IndexError("Invalid Memory Access")
      return None
    else:
      a = self.pop()
      b = self.pop()
      if int(a) == 0:
        self.CurrentLine = self.CurrentLine + int(b)
        return None
      else:
        return None
    
  def save(self,x):
    if (len(self.data)) == 0:
      self.error = "save"
      raise IndexError("Invalid Memory Access")
      return None
    else:
      a = self.pop()
      self.saved[int(x)] = a
      return None
  
  def get(self,x):
      a = self.saved[int(x)]
      self.push(a)
      return None
